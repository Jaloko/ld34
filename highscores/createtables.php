<?php
	$db = new SQLite3('database.db');
	$db->busyTimeout(5000);
	
	// Create database tables
	$db->exec('CREATE TABLE IF NOT EXISTS maps(
		id integer PRIMARY KEY NOT NULL,
		name text NOT NULL,
		width integer NOT NULL,
		height integer NOT NULL,
		amount_started integer,
		amount_completed integer,
		total_time_played integer)'
	);

	$db->exec('CREATE TABLE IF NOT EXISTS highscores(
		id integer PRIMARY KEY NOT NULL,
		name varchar(26) NOT NULL,
		time integer NOT NULL, 
		fk_map_id integer NOT NULL,
		FOREIGN KEY(fk_map_id) REFERENCES maps(id))'
	);

	// Insert maps
	$db->exec('INSERT OR IGNORE INTO maps(id, name, width, height, amount_started, amount_completed, total_time_played) VALUES (1, "Dungeon", 64, 64, 0, 0, 0)');
	$db->exec('INSERT OR IGNORE INTO maps(id, name, width, height, amount_started, amount_completed, total_time_played) VALUES (2, "Forest", 64, 64, 0, 0, 0)');
	$db->exec('INSERT OR IGNORE INTO maps(id, name, width, height, amount_started, amount_completed, total_time_played) VALUES (3, "Snow", 64, 64, 0, 0, 0)');
	$db->exec('INSERT OR IGNORE INTO maps(id, name, width, height, amount_started, amount_completed, total_time_played) VALUES (4, "Hell", 64, 64, 0, 0, 0)');
	$db->exec('INSERT OR IGNORE INTO maps(id, name, width, height, amount_started, amount_completed, total_time_played) VALUES (5, "Desert", 64, 64, 0, 0, 0)');

	$db->close();
?>