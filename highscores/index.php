<html>
	<head>
		<link rel="stylesheet" type="text/css" href="../css/main.css">
	</head>
	<body>
		<h1>Highscores</h1>
		<?php
/*		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);*/
			include 'createtables.php';
			$db = new SQLite3('database.db');

			function timeToString($time) {
				if($time >= 0 && $time < 3600) {
					return round($time / 60, 2) . "s";
				} else if($time >= 3600 && $time < 216000) {
					return floor($time / 60 / 60) . "m " . round(fmod($time / 60, 60), 2) . "s";
				} else if($time >= 216000 && $time < 5184000) {
					return floor($time / 60 / 60 / 60) . "h " . floor(fmod($time / 60 / 60, 60)) . "m " . round(fmod($time / 60, 60), 2) . "s";
				} else {
					return floor($time / 60 / 60 / 60 / 24) . "d " . floor(fmod($time / 60 / 60 / 60, 24)) . "h " . floor(fmod(($time / 60) / 60, 60)) . "m " . round(fmod(($time / 60), 60), 2) . "s";
				}
				return null;
			}

			function generateHighscoreTable($db, $mapId, $mapName, $started, $completed, $time) {
				// Query maps table for map name

				$table = '<div class="highscore-map"> <table>
					<thead>
						<tr class="primary title">
							<th colspan="3">' . $mapName . '</th>
						</tr>
						<tr class="header-item">
							<th colspan="3">Total started: ' . $started . '</th>
						</tr>
						<tr class="header-item">
							<th colspan="3">Total completed: ' . $completed . '</th>
						</tr>
						<tr class="header-item">
							<th colspan="3">Total time played: ' . timeToString($time) . '</th>
						</tr>
						<tr>
						</tr>
						<tr class="secondary">
							<th>Rank</th>
							<th>Name</th>
							<th>Time</th>
						</tr>
					</thead>
					<tbody>';

				// Create table rows based on database values
				$result = $db->query('SELECT * FROM highscores WHERE fk_map_id = ' . $mapId . ' ORDER BY time ASC');
				$rank = 1;
				while($row=$result->fetchArray()){
					$table .= '<tr>';
					$table .= '<td>' . $rank . '</td><td>' . $row['name'] . '</td><td>' . timeToString($row['time']) . '</td>';
					$table .= '</tr>';
					// Update rank
					$rank+=1;
				}
				
				// Check if table contains no data
				if($rank == 1) {
					$table .= '<tr><td colspan="3">No highscores yet!</td></tr>';
				}
						
				$table .= '</tbody>
				</table></div>';
				echo $table;
			}

			// Get all maps and generate the highscore tables
			$result = $db->query('SELECT * FROM maps');
			while($row=$result->fetchArray()){
				generateHighscoreTable(
					$db,
					$row['id'], 
					$row['name'], 
					$row['amount_started'], 
					$row['amount_completed'], 
					$row['total_time_played']
				);
			}

			$db->close();
		?>

	</body>
</html>