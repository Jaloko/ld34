<?php
	include 'createtables.php';

	$db = new SQLite3('database.db');
	$db->busyTimeout(5000);

	function getSlowestTime($db, $mapId) {
		$db = new SQLite3('database.db');
		$stmt = $db->prepare('SELECT max(time) AS slowest_time, count(time) AS entries FROM highscores WHERE fk_map_id =:mapid');
		$stmt->bindValue(':mapid', $mapId, SQLITE3_INTEGER);
		$result = $stmt->execute();
		return $result->fetchArray(SQLITE3_ASSOC);
	}

	if(!isset($_GET["mapId"])) {
		$result = $db->query('SELECT * FROM maps');
		$array = array();
		while($row=$result->fetchArray()){
			$innerArray = array();
			$innerArray['id'] = $row['id'];
			$innerArray['slowest_time'] = getSlowestTime($db, $row['id'])['slowest_time'];
			$innerArray['entries'] = getSlowestTime($db, $row['id'])['entries'];
			array_push($array, $innerArray);
		}
		echo json_encode($array);
	} else{
		$mapId = $_GET['mapId'];
		if(is_numeric($mapId)) {
			$innerArray = array();
			$innerArray['id'] = $mapId;
			$innerArray['slowest_time'] = getSlowestTime($db, $mapId)['slowest_time'];
			$innerArray['entries'] = getSlowestTime($db, $mapId)['entries'];
			echo json_encode($innerArray);
		}
	}

	$db->close();

?>