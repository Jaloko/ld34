<?php
	include 'createtables.php';

	$db = new SQLite3('database.db');
	$db->busyTimeout(5000);
	
	$data = json_decode($_POST['json'], true);
	// Validate name for inappropriate names

	// Validate time
	if($data['time'] < 1) {
		die();
	}

	// Validate map foreign key
	$stmt = $db->prepare('SELECT COUNT(*) FROM maps WHERE id = :mapid');
	$stmt->bindValue(':mapid', $data['map'], SQLITE3_INTEGER);
	$result = $stmt->execute();
	$size;
	while($row=$result->fetchArray()){
	   $size = $row[0];
	}
	if($size != 1) {
		die();
	}

	// Get number of entries
	$stmt = $db->prepare('SELECT COUNT(*) FROM highscores WHERE fk_map_id = :mapid');
	$stmt->bindValue(':mapid', $data['map'], SQLITE3_INTEGER);
	$result = $stmt->execute();
	$size;
	while($row=$result->fetchArray()){
	   $size = $row[0];
	}
	// First check number of entries - if less than 10 add the entry
	if($size >= 10) {
		$stmt = $db->prepare('SELECT * FROM highscores WHERE fk_map_id =:mapid ORDER BY time ASC');
		$stmt->bindValue(':mapid', $data['map'], SQLITE3_INTEGER);
		$result = $stmt->execute();

		$array = array();
		while($row=$result->fetchArray()){
		   $size = array_push($array, $row);
		}
		$slowestTimeId = $array[sizeof($array) - 1]['id'];
		$stmt = $db->prepare('UPDATE highscores SET name=:name, time=:time WHERE id=:slowestTimeId');
		$stmt->bindValue(':name', $data['name'], SQLITE3_TEXT);
		$stmt->bindValue(':time', $data['time'], SQLITE3_INTEGER);
		$stmt->bindValue(':slowestTimeId', $slowestTimeId, SQLITE3_INTEGER);
		$result = $stmt->execute();
	} else {
		$stmt = $db->prepare('INSERT INTO highscores(name, time, fk_map_id) VALUES (:name, :time, :map)');
		$stmt->bindValue(':name', $data['name'], SQLITE3_TEXT);
		$stmt->bindValue(':time', $data['time'], SQLITE3_INTEGER);
		$stmt->bindValue(':map', $data['map'], SQLITE3_INTEGER);
		$result = $stmt->execute();
	}

	$db->close();
?>