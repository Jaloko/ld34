<?php
	include 'createtables.php';

	$db = new SQLite3('database.db');
	$db->busyTimeout(5000);
	
	if(!isset($_GET["mapId"])) {
		die();
	} else {
		$mapId = $_GET["mapId"];

		// Validate map id
		$stmt = $db->prepare('SELECT COUNT(*) FROM maps WHERE id = :mapid');
		$stmt->bindValue(':mapid', $mapId, SQLITE3_INTEGER);
		$result = $stmt->execute();
		$size;
		while($row=$result->fetchArray()){
		   $size = $row[0];
		}
		if($size != 1) {
			die();
		}

		// Increment started
		$stmt = $db->prepare('UPDATE maps SET amount_started = (amount_started + 1) WHERE id=:mapId');
		$stmt->bindValue(':mapId', $mapId, SQLITE3_INTEGER);
		$result = $stmt->execute();
	}

	$db->close();
?>   