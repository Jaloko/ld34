<?php
	include 'createtables.php';

	$db = new SQLite3('database.db');
	$db->busyTimeout(5000);
	
	$data = json_decode($_POST['json'], true);

	if($data == null) {
		die();
	}

	// Validate time
	if($data['time'] < 1) {
		die();
	}

	// Validate map foreign key
	$stmt = $db->prepare('SELECT COUNT(*) FROM maps WHERE id = :mapid');
	$stmt->bindValue(':mapid', $data['map'], SQLITE3_INTEGER);
	$result = $stmt->execute();
	$size;
	while($row=$result->fetchArray()){
	   $size = $row[0];
	}
	if($size != 1) {
		die();
	}

	// Increment completed
	$stmt = $db->prepare('UPDATE maps SET amount_completed = (amount_completed + 1), total_time_played = (total_time_played + :newTime) WHERE id=:mapId');
	$stmt->bindValue(':newTime', $data['time'], SQLITE3_INTEGER);
	$stmt->bindValue(':mapId', $data['map'], SQLITE3_INTEGER);
	$result = $stmt->execute();

	$db->close();
?>   