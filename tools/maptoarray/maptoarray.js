(function() {
	function readLevelFromUrl(url) {
		return new Promise(function(resolve, reject) {
			var levelImg = new Image();
			levelImg.addEventListener('load', function() {
				// When the image loads draw it onto a canvas so that we can read the pixels
				var canvas = document.createElement('canvas');
				var width = levelImg.width;
				var height = levelImg.height;
				canvas.width = width;
				canvas.height = height;

				var level = {
					width: width,
					height: height,
					playerSpawn: {
						x: 0,
						y: 0
					},
					winPositions: [],
					data: [],
					objects: {}
				};

				var ctx = canvas.getContext('2d');
				ctx.drawImage(levelImg, 0, 0);

				var imageData = ctx.getImageData(0, 0, width, height).data;
				// Read each RGBA pixel
				for (var y = 0; y < height; y++) {
					for (var x = 0; x < width; x++) {
						var i = y * width + x;

						// Red channel represents tiles.
						level.data[i] = imageData[i*4];

						// Green channel currently indicates win position
						if (imageData[i*4 + 1] == 255) {
							level.winPositions.push({x: x, y: y});
						}

						// Blue channel currently indicates spawn location
						if (imageData[i*4 + 2] == 255) {
							level.playerSpawn.x = x;
							level.playerSpawn.y = y;
						} else if (imageData[i*4 + 2] == 128) {
							if (!level.objects[x]) {
								level.objects[x] = {};
							}
							level.objects[x][y] = {type: 0};
						}
					}
				}
				resolve(level);
			});
			levelImg.src = url;
		});
	}

	document.getElementById('levelConvertForm').addEventListener('submit', function(e) {
		e.preventDefault();
		var levelCount = Number(document.getElementById('levelCount').value);

		var levelUrls = [];
		for (var i = 0; i < levelCount; i++) {
			levelUrls[i] = 'levels/Level' + (i + 1) + '.png';
		}
		var promisedlevelDatas = levelUrls.map(function(url) {
			return readLevelFromUrl(url);
		});
		Promise.all(promisedlevelDatas).then(function(levelDatas) {
		    	document.getElementById('output').innerHTML = 'var LEVEL_DATA=' + JSON.stringify(levelDatas) + ';';
		    });
		});
})();
