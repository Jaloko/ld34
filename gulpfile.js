var gulp = require("gulp");
var sourcemaps = require("gulp-sourcemaps");
var concat = require("gulp-concat");
var uglify = require('gulp-uglify');
var del = require('del');

var config = {
	js: [
		'js/*.js'
	],
	tasks: {
		default: ['clean', 'final-build'],
		dev: ['clean', 'dev-build']
	}
}

gulp.task('clean', function(){
    return del(['js/bundle.js', 'js/bundle.js.map']);
});

gulp.task("dev-build", ['clean'], function () {
  return gulp.src(config.js)
    .pipe(sourcemaps.init())
    .pipe(concat("bundle.js"))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("js"));
});

gulp.task("final-build", ['clean'], function () {
  return gulp.src(config.js)
    .pipe(concat("bundle.js"))
    .pipe(uglify())
    .pipe(gulp.dest("js"));
});

gulp.task('default', config.tasks.default);
gulp.task('dev', config.tasks.dev);