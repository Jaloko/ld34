var Utils = { };

/**
 * 	Returns the primary canvas element
 */
Utils.getCanvas = function() {
	return document.getElementById('canvas');
};

/**
 * 	Returns the primary canvas element width and height
 */
Utils.getCanvasSize = function() {
	return {
		width: this.getCanvas().width,
		height: this.getCanvas().height
	};
};

/**
 * 	Returns the primary canvas element center x and y
 */
Utils.getCanvasCenter = function() {
	return {
		x: this.getCanvas().width / 2,
		y: this.getCanvas().height / 2
	};
};

/**
 * Divides the height by an amount of divisions and returns the resulting positions
 */
Utils.divideHeight = function(numberOfDivisions) {
	var size = this.getCanvasSize();
	var division = (size.height) / numberOfDivisions;

	var array = [];
	for(var i = 0; i < numberOfDivisions; i++) {
		array.push((division * i) + (division / 2));
	}
	return array;
}

/**
 * RCX - Render Center X
 * Returns the x position that will draw an object in the center of the canvas
 */
Utils.getRCX = function(width) {
	var center = this.getCanvasCenter();
	return center.x - (width / 2);
};

/**
 * RCY - Render Center Y
 * Returns the y position that will draw an object in the center of the canvas
 */
Utils.getRCY = function(height) {
	var center = this.getCanvasCenter();
	return center.y - (height / 2);
};

/**
 * RC - Render Center 
 * Returns the x and y position that will draw an object in the center of the canvas
 */
Utils.getRC = function(width, height) {
	return {
		x: getRCX(width),
		y: getRCY(height)
	};
};

/**
 * Converts an x and y coordinate into an array index position
 */
Utils.convertToTileIndex = function(width, x, y) {
	return ((y / 32)|0) * width + ((x / 32)|0);
}

/**
 * Gets full local url
 */
Utils.getLocalURL = function() {
	var url = window.location.href
	url = url.substring(0, url.lastIndexOf("/") + 1);
	return url;
}

/**
 * Gets JSON from a local path
 */
Utils.getJSON = function(path, success, error)
{
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function()
    {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                if (success)
                    success(JSON.parse(xhr.responseText));
            } else {
                if (error)
                    error(xhr);
            }
        }
    };
    xhr.open("GET", path, true);
    xhr.send();
}

/**
 * Sends an xmlhttp request
 */
Utils.sendPostRequest = function(json, url) {
	if (window.XMLHttpRequest) {
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function () {
			if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
				//alert(xmlhttp.responseText);
			}
		};
		xmlhttp.open("POST", url);
		xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		if(json != null) {
			var data = "json=" + JSON.stringify(json);
			xmlhttp.send(data);
		} else {
			xmlhttp.send();
		}
	}
}

/**
 * Converts a frame based time to a string
 */
Utils.timeToString = function(time) {
	if(time >= 0 && time < 3600) {
		return (time / 60).toFixed(2) + "s";
	} else if(time >= 3600 && time < 216000) {
		return Math.floor(((time / 60) / 60)) + "m " + ((time / 60) % 60).toFixed(2) + "s";
	} else if( time >= 216000 && time < 5184000) {
		return Math.floor(((time / 60) / 60) / 60) + "h " + Math.floor(((time / 60) / 60) % 60) + "m " + ((time / 60) % 60).toFixed(2) + "s";
	} else {
		return Math.floor((((time / 60) / 60) / 60) / 24) + "d " + Math.floor((((time / 60) / 60) / 60) % 24) + "h " + Math.floor(((time / 60) / 60) % 60) + "m " + ((time / 60) % 60).toFixed(2) + "s";
	}
	return null;
}

/**
 * Gets a cookie based on a name
 */
Utils.getCookie = function(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}