var ctx;
var game;
var keys = [65565];

var GameStates = {
	MENU: 0,
	PLAYING: 1,
	HIGHSCORES: 2,
	INSTRUCTIONS: 3
}
var gameState = GameStates.MENU;

function init() {
	Sounds.muteAll();
	// Base on cookie check if sounds should be muted or not.
	Sounds.init();
	//Initialization here
	var canvas = document.getElementById('canvas');
	ctx = canvas.getContext('2d');

	addKeyBindings(); 

	Sounds.menu.play();
	Textures.loadTextures(update);
}

function update() {
	// Update code here
	checkKeys();
	switch(gameState) {
		case GameStates.PLAYING:
			game.update();
			break;
	}

	
	render();
	requestAnimationFrame(update);
}

function render() {
	// Keep the canvas focused;
	// Drawing code here
	// Clear canvas
	ctx.canvas.focus();
	ctx.fillStyle = '#000000';
	ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);

	switch(gameState) {
		case GameStates.MENU:
			Menu.render(ctx);
			break;
		case GameStates.INSTRUCTIONS:
			Instructions.render(ctx);
			break;
		case GameStates.PLAYING:
			game.render(ctx);
			break;
		case GameStates.HIGHSCORES:
			Highscores.render(ctx);
			break;
	}
}

function checkKeys() {
	// Gameplay keys go here so that there is no input lag
	switch(gameState) {
		case GameStates.PLAYING:
			game.checkKeys(keys);
			break;
	}
}

function addKeyBindings(){
	window.addEventListener('keydown', function(e){
		keys[e.keyCode] = true;
		// Menu keys stay here because we only want it to react each key press
		switch(gameState) {
			case GameStates.MENU:
				Menu.onKeyDown(e);
				break;
			case GameStates.INSTRUCTIONS:
				Instructions.onKeyDown(e);
				break;
			case GameStates.HIGHSCORES:
				Highscores.onKeyDown(e);
				break;
		}
	});

	window.addEventListener('keyup', function(e){
		keys[e.keyCode] = false;
		switch(gameState) {
			case GameStates.HIGHSCORES:
				Highscores.onKeyUp(e);
				break;
		}
	});
}

