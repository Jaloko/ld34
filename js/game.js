function Game(level) {
	this.player = Object.create(Player);
	this.map = Object.create(Map);
	this.map.currentLevel = level;
	this.map.initLevel();
	this.player.initLocation(this.map);

	this.init(level);
}

Game.prototype.init = function(level) {
	switch(level) {
		case 0:
		// Dungeon
		Sounds.dungeon.play();
			break;
		case 1:
		// Forest
		Sounds.forest.play();
			break;
		case 2:
		// Snow
		Sounds.snow.play();
			break;
		case 3:
		// Hell
		Sounds.hell.play();
			break;
		case 4:
		// Desert
		Sounds.desert.play();
			break;
	}
}

Game.prototype.update = function() {
	this.player.update();
}

Game.prototype.render = function(ctx) {
	this.map.render(ctx, this.player.x, this.player.y);
	this.player.render(ctx);
}

Game.prototype.checkKeys = function(keys) {
	if(keys[90]) {  // Z
		this.map.rotateMap(true);
	} else if(keys[88]) { // X
		this.map.rotateMap(false);
	}
}

