var Textures = {
	spritesheets: {},
	filePaths: {
		'tiles': 'img/tiles.png',
		'player': 'img/player.png',
		'fire': 'img/fire.png',
	}
};

Textures.loadTextures = function(callback) {
	var imageCount = 0;
	var t = this;

	function onImageLoad() {
		imageCount++;
		if(imageCount == Object.keys(t.filePaths).length) {
			callback();
		}
	}

	for (var key in this.filePaths) {
		this.spritesheets[key] = new Image();
		this.spritesheets[key].onload = onImageLoad;
		this.spritesheets[key].src = this.filePaths[key];
	}
};
