var Player = {
	x: 0,
	y: 0,
	time: 0,

	initLocation: function(map) {
		var cl = map.levels[map.currentLevel];
		this.x = cl.playerSpawn.x * 32;
		this.y = cl.playerSpawn.y * 32;
	},

	update: function() {
		this.time++;
		if(game.map.rotationChanged == false) {
			this.walk();
		}
	},

	// Handles the walk function of the player
	walk: function(){
		var nextX = this.x;
		var nextY = this.y;
		// Collision detection
		var cl = game.map.levels[game.map.currentLevel];

		var collisionTestTiles;

		for(var i = 0; i < 3; i++) {
			switch(game.map.rotationPosition) {
				case 0:
					nextY -= 1;
					break;
				case 1:
					nextX -= 1;
					break;
				case 2:
					nextY += 1;
					break;
				case 3:
					nextX += 1;
					break;
			}

			var dontMove = true;
			var collisionTestTiles = [];
			switch(game.map.rotationPosition) {
				case 0:
					collisionTestTiles.push({
						x: this.x,
						y: this.y - 1
					});
					collisionTestTiles.push({
						x: this.x + 31,
						y: this.y - 1
					});
					if(cl.data[Utils.convertToTileIndex(cl.width, this.x, this.y - 1)] < 128 &&
						cl.data[Utils.convertToTileIndex(cl.width, this.x + 31, this.y - 1)] < 128) {
						dontMove = false;
					}
					break;
				case 1:
					collisionTestTiles.push({
						x: this.x - 1,
						y: this.y
					});
					collisionTestTiles.push({
						x: this.x - 1,
						y: this.y + 31
					});
					if(cl.data[Utils.convertToTileIndex(cl.width, this.x - 1, this.y)] < 128 && 
						cl.data[Utils.convertToTileIndex(cl.width, this.x - 1, this.y + 31)] < 128) {
						dontMove = false;
					}
					break;
				case 2:
					collisionTestTiles.push({
						x: this.x,
						y: this.y + 32
					});
					collisionTestTiles.push({
						x: this.x + 31,
						y: this.y + 32
					});
					if(cl.data[Utils.convertToTileIndex(cl.width, this.x , this.y + 32)] < 128 &&
						cl.data[Utils.convertToTileIndex(cl.width, this.x + 31, this.y + 32)] < 128) {
						dontMove = false;
					}
					break;
				case 3:
					collisionTestTiles.push({
						x: this.x + 32,
						y: this.y
					});
					collisionTestTiles.push({
						x: this.x + 32,
						y: this.y + 31
					});
					if(cl.data[Utils.convertToTileIndex(cl.width, this.x + 32, this.y)] < 128 &&
						cl.data[Utils.convertToTileIndex(cl.width, this.x + 32 , this.y + 31)] < 128) {
						dontMove = false;
					}
					break;
			}

			if(dontMove == false) {
				this.x = nextX;
				this.y = nextY;
			}
		}

		// Test win conditions
		for(var i = 0; i < cl.winPositions.length; i++) {
			if(cl.winPositions[i].x == ((this.x / 32) | 0) &&
				cl.winPositions[i].y == ((this.y / 32) | 0)) {
				// If Win condition met
				// Send map completed statistic

				var json = { time: this.time, map: game.map.getCurrentId() };
				var url = Utils.getLocalURL() + "highscores/submitcompleted.php";
				Utils.sendPostRequest(json, url);

				var p = this;
				Utils.getJSON(
					Utils.getLocalURL() + 'highscores/getslowesttimes.php?mapId=' + game.map.getCurrentId(), 
					// Success
					// Data is json containing map ids and their lowest highscore
					function(data) {
						// Check if player score is higher than the lowest highscore of map
						// If it is prompt the user to enter their name then submit their score
						if(parseInt(data['entries']) < 10 || p.time < parseInt(data['slowest_time'])) {
							Highscores.clearInput();
							gameState = GameStates.HIGHSCORES;
							Highscores.selectedItem = HighscoreItems.NEW_HIGHSCORE;
							Sounds.newHighscore.play();
						} else {
							gameState = GameStates.HIGHSCORES;
							Highscores.selectedItem = HighscoreItems.NO_NEW_HIGHSCORE;
						}
		
						// End
					},
					// Error
					function(xhr) {
						console.error(xhr);
					}
				);
				gameState = GameStates.HIGHSCORES;
				break;
			}
		}

		//Test map object collisions
		for (var i = 0; i < collisionTestTiles.length; i++) {
			var mapX = ((collisionTestTiles[i].x / 32) | 0);
			var mapY = ((collisionTestTiles[i].y / 32) | 0);

			if (game.map.objectsState[mapX] && game.map.objectsState[mapX][mapY]) {
				var os = game.map.objectsState[mapX][mapY];
				var ot = ObjectTypes[os.type];
				ot.onCollide(mapX, mapY);
				ot.playSound();
			}
		}
	},

	render: function(ctx) {
		// Draw player sprite
		//ctx.drawImage(Textures.spritesheets.player, Utils.getRCX(32), Utils.getRCY(32), 32, 32);

		var animationPeriod = 27;
		var animationFrames = 3;

		ctx.drawImage(Textures.spritesheets.player,
			//Source
			(((game.map.objectAnimationFrame % animationPeriod) / (animationPeriod / animationFrames)) | 0 )  * 16, 0, 16, 16, 
			// Destination
			Utils.getRCX(32),
			Utils.getRCY(32),
			32, 32);
		// Draw players score
		ctx.font = "25px Roboto Mono";
		ctx.fillStyle = "#ffffff";
		ctx.textAlign="left"; 
		ctx.fillText('Current time: ' + Utils.timeToString(this.time), 0, 25);
		27
	}

};

