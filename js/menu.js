var MainMenuItems = [
	{
		text: 'Start',
		action: function() {
			Menu.selectedItem = 0;
			Menu.currentMenu = MapMenuItems;
		}
	},
	{
		text: 'Highscores',
		action: function() {
			// Dont need a game state for this option
			var url = Utils.getLocalURL() + 'highscores';

			window.open(url,'targetWindow',
			                'toolbar=no,' +
			                'location=no,' +
			                'status=no,' +
			                'menubar=no,' +
			                'scrollbars=yes,' +
			                'resizable=yes,' +
			                'width=1280,' +
			                'height=720');
		}

	},
	{
		text: 'Instructions',
		action: function() {
			gameState = GameStates.INSTRUCTIONS;
			Menu.selectedItem = 0;
		}
	}
];

function selectMap(num) {
	Sounds.stopAll();
	gameState = GameStates.PLAYING;
	game = new Game(num);

	var url = Utils.getLocalURL() + "highscores/submitstarted.php?mapId=" + game.map.getCurrentId();
	Utils.sendPostRequest(null, url);
}

var MapMenuItems = [
	{
		text: 'Dungeon',
		action: function() {
			selectMap(0);
		}
	},
	{
		text: 'Forest',
		action: function() {
			selectMap(1);
		}
	},
	{
		text: 'Snow',
		action: function() {
			selectMap(2);
		}
	},
	{
		text: 'Hell',
		action: function() {
			selectMap(3);
		}
	},
	{
		text: 'Desert',
		action: function() {
			selectMap(4);
		}
	},
	{
		text: 'Back',
		action: function() {
			Menu.currentMenu = MainMenuItems;
			Menu.selectedItem = 0;
		}
	}
];

var Menu = {
	currentMenu: MainMenuItems,
	selectedItem: 0
};

Menu.changeItem = function(val) {
	this.selectedItem += val;
	if (this.selectedItem < 0) {
		this.selectedItem = this.currentMenu.length - 1;
	} else if (this.selectedItem >= this.currentMenu.length) {
		this.selectedItem = 0;
	}
}

Menu.render = function(ctx) {
	// Should only need to do this once
	var menuPositions = Utils.divideHeight(this.currentMenu.length + 1);

	var center = Utils.getCanvasCenter();

	ctx.fillStyle = '#ffffff';
	ctx.textAlign="center"; 

	ctx.font = "700 48px Roboto Mono";
	ctx.fillText('The Lost Wanderer', center.x,  menuPositions[0]);

	ctx.font = "36px Roboto Mono";
	for (var i = 0; i < this.currentMenu.length; i++) {
		var itemText;
		if (this.selectedItem == i) {
			itemText = '< ' + this.currentMenu[i].text + ' >';
		} else {
			itemText = this.currentMenu[i].text;
		}
		ctx.fillText(itemText, center.x,  menuPositions[i+1]);
	}

};

Menu.select = function() {
	this.currentMenu[this.selectedItem].action();
}

Menu.onKeyDown = function(e) {
	switch(e.keyCode) {
		case 38: // Up Arrow 
			Menu.changeItem(-1);
			break;
		case 40: // Down Arrow 
			Menu.changeItem(+1);
			break;
		case 13: // Enter Key
			Menu.select();
			break;
	}
}