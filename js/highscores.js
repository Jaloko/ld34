var HighscoreItems = {
	NONE: 0,
	NEW_HIGHSCORE: 1,
	NO_NEW_HIGHSCORE: 2
};

var Highscores = {
	selectedItem: HighscoreItems.NONE,
	tempName: ""
};

Highscores.clearInput = function() {
	document.getElementById('name').value = "";
};

Highscores.render = function(ctx) {
	var center = Utils.getCanvasCenter();
	switch(this.selectedItem) {
		case HighscoreItems.NONE:
			break;
		case HighscoreItems.NEW_HIGHSCORE:
			var positions = Utils.divideHeight(4);
			ctx.fillStyle = '#ffffff';
			ctx.textAlign="center"; 
			ctx.font = "30px Roboto Mono";
			ctx.fillText('New Highscore! ' + Utils.timeToString(game.player.time), center.x,  positions[0]);
			ctx.font = "25px Roboto Mono";
			ctx.fillText('Enter a name:', center.x,  positions[1]);
			ctx.fillText(this.tempName, center.x,  positions[2]);
			ctx.fillText('Press Enter To Submit', center.x,  positions[3]);
			break;
		case HighscoreItems.NO_NEW_HIGHSCORE:
			var positions = Utils.divideHeight(2);
			ctx.fillStyle = '#ffffff';
			ctx.textAlign="center"; 
			ctx.font = "30px Roboto Mono";
			ctx.fillText('You finished with a score of: ' + Utils.timeToString(game.player.time), center.x,  positions[0]);
			ctx.font = "25px Roboto Mono";
			ctx.fillText('Press Enter For Main Menu', center.x,  positions[1]);
			break;
	}

};

Highscores.submitScore = function() {
	var json = {name: this.tempName, time: game.player.time, map: game.map.getCurrentId() };
	var url = Utils.getLocalURL() + "highscores/submithighscore.php";
	Utils.sendPostRequest(json, url);
}

Highscores.onKeyDown = function(e) {
	switch(this.selectedItem) {
		case HighscoreItems.NEW_HIGHSCORE:
			this.tempName = document.getElementById('name').value;
			document.getElementById('name').focus();
			break;
	}

	switch(e.keyCode) {
		case 13: // Enter Key
			switch(this.selectedItem) {
				case HighscoreItems.NEW_HIGHSCORE:
					// Now take player to home page or map selection
					this.selectedItem = HighscoreItems.NONE;
					gameState = GameStates.MENU;
					this.submitScore();
					this.tempName = "";
					break;
				case HighscoreItems.NO_NEW_HIGHSCORE:
					this.tempName = "";
					this.selectedItem = HighscoreItems.NONE;
					gameState = GameStates.MENU;
					break;
			}
			Sounds.stopAll();
			Sounds.menu.play();
			break;
	}
}

Highscores.onKeyUp = function(e) {
	switch(this.selectedItem) {
		case HighscoreItems.NEW_HIGHSCORE:
			this.tempName = document.getElementById('name').value;
			break;
	}
}
