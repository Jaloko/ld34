var Map = {
	// 0 = up, 1 = left, 2 = down, 3 = right
	rotationPosition: 0,
	rotationFrame: 90,
	rotationChanged: false,
	rotateLeft: false,
	currentLevel: 0,
	levelMax: 1,
	levels: LEVEL_DATA,

	objectAnimationFrame: 0,
	objectsState: {},

	torchTimeRemaining: 0,
	torchGrowFrame: 60,

	initLevel: function() {
		// Make a copy of the map objects. We'll need to modify these in response to game events
		// and don't want to change the original global copy.
		this.objectsState = JSON.parse(JSON.stringify(this.levels[this.currentLevel].objects));
	},

	// Handles rotation map
	rotateMap: function(rotateLeft) {
		if(!this.rotationChanged) {
			this.rotationFrame = 0;
			this.rotateLeft = rotateLeft;
			this.rotationChanged = true;
			if(this.rotateLeft && this.rotationPosition === 3){
				this.rotationPosition = 0;
			}
			else if(!this.rotateLeft && this.rotationPosition === 0){
				this.rotationPosition = 3;
			}
			else{
				if(this.rotateLeft) {
					this.rotationPosition++;
				} else {
					this.rotationPosition--;
				}

			}
		}
	},

	getRotationAmount: function(val) {
		var m = this;
		function handleRotation() {
			if(m.rotationChanged) {
				m.rotationFrame+=5;
			}
			if(m.rotationFrame >= 90) {
				m.rotationChanged = false;
			}
		}
		handleRotation();
		switch(val) {
			case 0:
				if(this.rotateLeft) {
					return 270 + this.rotationFrame;
				} else {
					return 0 + (90 - this.rotationFrame);
				}					
			case 1:
				if(this.rotateLeft) {
					return 0 + this.rotationFrame;
				} else {
					return 90 + (90 - this.rotationFrame);
				}	
			case 2:
				if(this.rotateLeft) {
					return 90 + this.rotationFrame;
				} else {
					return 180 + (90 - this.rotationFrame);
				}	
			case 3:
				if(this.rotateLeft) {
					return 180 + this.rotationFrame;
				} else {
					return 270 + (90 - this.rotationFrame);
				}	
		}
		return null;
	},

	getCurrentId: function() {
		return this.currentLevel + 1;
	},

	getViewDistance: function() {
		if(this.torchTimeRemaining > 0) {
			if(!(this.torchGrowFrame >= 100)) {
				this.torchGrowFrame+=1;
			}
			
		} else {
			if(!(this.torchGrowFrame <= 60)) {
				this.torchGrowFrame-=1;
			}
		}
		return this.torchGrowFrame / 10;
	},

	render: function(ctx, playerX, playerY) {
		ctx.mozImageSmoothingEnabled = false;
		ctx.webkitImageSmoothingEnabled = false;
		ctx.msImageSmoothingEnabled = false;
		ctx.imageSmoothingEnabled = false;

		var viewDistance = Math.floor(this.getViewDistance());

		ctx.save();
		ctx.beginPath();
		var center = Utils.getCanvasCenter();
		ctx.arc(center.x,center.y,(this.getViewDistance() - 1) * 32,0,Math.PI*2,true);
		ctx.clip();

		ctx.translate(Utils.getCanvasCenter().x, Utils.getCanvasCenter().y);
		ctx.rotate(this.getRotationAmount(this.rotationPosition)*Math.PI/180);
		ctx.translate(-Utils.getCanvasCenter().x, -Utils.getCanvasCenter().y);

		var cl = this.levels[this.currentLevel];
		for (var y = viewDistance * -1; y <= viewDistance; y++) {
			// Get the current map Y coordinate
			var mapY = ((playerY / 32) | 0 ) + y;
			if (mapY < 0 || mapY >= cl.height) {
				continue;
			}
			for (var x = viewDistance * -1; x <= viewDistance; x++) {
				var mapX = ((playerX / 32) | 0 ) + x;
				if (mapX < 0 || mapX >= cl.width) {
					continue;
				}
				var tile = cl.data[mapY * cl.width + mapX];
				var tileX = (tile % 16) * 16;
				var tileY = ((tile / 16) | 0) * 16;

				ctx.drawImage(Textures.spritesheets.tiles,
					//Source
					tileX, tileY, 16, 16, 
					// Destination
					Utils.getRCX(32) + x * 32 + ((playerX % 32) * -1),
					Utils.getRCY(32) + y * 32 + ((playerY % 32) * -1),
					32, 32);

				if (this.objectsState[mapX] && this.objectsState[mapX][mapY]) {
					var obj = ObjectTypes[this.objectsState[mapX][mapY].type];
					ctx.drawImage(Textures.spritesheets[obj.spritesheet],
						//Source
						(((this.objectAnimationFrame % obj.animationPeriod) / (obj.animationPeriod / obj.animationFrames)) | 0 )  * 16, 0, 16, 16, 
						// Destination
						Utils.getRCX(32) + x * 32 + ((playerX % 32) * -1),
						Utils.getRCY(32) + y * 32 + ((playerY % 32) * -1),
						32, 32);
				}
			}
		}
		ctx.restore();

		this.objectAnimationFrame++;
		if (this.torchTimeRemaining > 0) {
			this.torchTimeRemaining--;
			// Draw torch time
			ctx.font = "25px Roboto Mono";
			ctx.fillStyle = "#ffffff";
			ctx.textAlign="left"; 
			ctx.fillText('Torch time: ' + Utils.timeToString(this.torchTimeRemaining), 0, 55);
		}
	}
};
