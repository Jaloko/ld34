var Instructions = { };

Instructions.render = function(ctx) {
	var instructionPositions = Utils.divideHeight(6);
	var center = Utils.getCanvasCenter();

	ctx.fillStyle = '#ffffff';
	ctx.textAlign="center"; 

	ctx.font = "700 48px Roboto Mono";
	ctx.fillText('The Lost Wanderer', center.x,  instructionPositions[0]);

	ctx.fillStyle = '#ffffff';
	ctx.font = "36px Roboto Mono";
	ctx.fillText('Instructions', center.x,  instructionPositions[1]);
	ctx.font = "30px Roboto Mono";
	ctx.fillText('The player automatically moves up the screen.', center.x,  instructionPositions[2]);
	ctx.fillText('Press Z and X to rotate the map left and right.', center.x,  instructionPositions[3]);
	ctx.fillText('Light your torch on fires to grow your view.', center.x,  instructionPositions[4]);
	ctx.fillText('< Back >', center.x,  instructionPositions[5]);
};

Instructions.onKeyDown = function(e) {
	switch(e.keyCode) {
		case 13: // Enter Key
			gameState = GameStates.MENU;
			break;
	}
}