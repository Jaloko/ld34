var ObjectTypes = [
	{
		spritesheet: 'fire',
		animationPeriod: 120,
		animationFrames: 5,
		onCollide: function(x, y) {
			console.log('fire onCollide');
			game.map.objectsState[x][y] = null;
			game.map.torchTimeRemaining += 900;
		},
		playSound: function() {
			Sounds.firePickup.play();
		}
	}
];
