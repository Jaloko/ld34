var Sounds = {
	newHighscore : new Audio("sound/new_highscore.wav"),
	firePickup : new Audio("sound/fire_pickup.wav"),
	dungeon : new Audio("sound/Dungeon.mp3"),
	forest : new Audio("sound/Forest.mp3"),
	snow : new Audio("sound/Snow.mp3"),
	hell : new Audio("sound/Hell.mp3"),
	desert : new Audio("sound/Desert.mp3"),
	menu: new Audio("sound/Menu.mp3"),
	allMuted: false
};

Sounds.stopAll = function() {
	this.stopTrack(this.newHighscore);
	this.stopTrack(this.firePickup);
	this.stopTrack(this.dungeon);
	this.stopTrack(this.forest);
	this.stopTrack(this.snow);
	this.stopTrack(this.hell);
	this.stopTrack(this.desert);
	this.stopTrack(this.menu);
};

Sounds.init = function() {
	if(Utils.getCookie("muted") != null) {
		if(Utils.getCookie("muted") == 1) {

		} else {
			this.muteAll();
		}
	} else {
		this.muteAll();
	}
}

Sounds.muteAll = function() {
	this.allMuted = !this.allMuted;
	this.newHighscore.muted = this.allMuted;
	this.firePickup.muted = this.allMuted;
	this.dungeon.muted = this.allMuted;
	this.forest.muted = this.allMuted;
	this.snow.muted = this.allMuted;
	this.hell.muted = this.allMuted;
	this.desert.muted = this.allMuted;
	this.menu.muted = this.allMuted;
}

Sounds.muteAllWithCookie = function() {
	Sounds.muteAll();
	if(this.allMuted) {
		document.cookie="muted=1";
	} else {
		document.cookie="muted=0";
	}
}

Sounds.stopTrack = function(track) {
	track.pause();
	track.currentTime = 0;
}

Sounds.newHighscore.volume = 0.5;
Sounds.firePickup.volume = 0.5;

Sounds.dungeon.addEventListener('ended', function() {
    this.currentTime = 0;
    this.play();
}, false);
Sounds.dungeon.volume = 0.5;

Sounds.forest.addEventListener('ended', function() {
    this.currentTime = 0;
    this.play();
}, false);
Sounds.forest.volume = 0.20;

Sounds.snow.addEventListener('ended', function() {
    this.currentTime = 0;
    this.play();
}, false);
Sounds.snow.volume = 0.2;

Sounds.hell.addEventListener('ended', function() {
    this.currentTime = 0;
    this.play();
}, false);
Sounds.hell.volume = 0.6;

Sounds.desert.addEventListener('ended', function() {
    this.currentTime = 0;
    this.play();
}, false);
Sounds.desert.volume = 0.5;

Sounds.menu.addEventListener('ended', function() {
    this.currentTime = 0;
    this.play();
}, false);
Sounds.menu.volume = 0.4;
